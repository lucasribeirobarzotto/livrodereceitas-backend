package br.com.livrodereceitasbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LivroDeReceitasBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(LivroDeReceitasBackendApplication.class, args);
	}

}
